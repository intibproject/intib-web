@extends('layouts.layout')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($menuAccess->isWrite=='Yes')
        <a class="btn bg-green btn-app" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-plus"></i> @lang('custom.security.user.add')
        </a>
    @endif
    @if($menuAccess->isDelete=='Yes')
        <a class="btn bg-red btn-app" id="deleteTriger">
            <i class="fa fa-times"></i> @lang('custom.security.user.delete')
        </a>
    @endif

    <div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-hover" id="users-table">
                <thead>
                <tr>
                    <th><input type="checkbox" name="bulkDelete" id="bulkDelete"/></th>
                    <th>@lang('custom.security.user.username')</th>
                    <th>@lang('custom.security.user.fullname')</th>
                    <th>@lang('custom.security.user.jobtitle')</th>
                    <th>@lang('custom.security.user.email')</th>
                    <th>@lang('custom.security.user.phone')</th>
                    <th>@lang('custom.security.user.role')</th>
                    <th>@lang('custom.registereddate')</th>
                    <th>@lang('custom.action')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Add Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('custom.security.user.add')</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{route('addSaveUser')}}" method="POST" id="addUserForm">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <div class="box-body">
                            <div class="form-group">
                                <label for="in_username" class="col-sm-2 control-label">@lang('custom.security.user.username')</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="in_username" name="in_username" placeholder="@lang('custom.security.user.username')" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_pass" class="col-sm-2 control-label">@lang('custom.security.user.password')</label>

                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="in_pass" name="in_pass" placeholder="@lang('custom.security.user.password')" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_confpass" class="col-sm-2 control-label">@lang('custom.security.user.confirmpassword')</label>

                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="in_confpass" name="in_confpass" placeholder="@lang('custom.security.user.confirmpassword')" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_fullname" class="col-sm-2 control-label">@lang('custom.security.user.fullname')</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="in_fullname" name="in_fullname" placeholder="@lang('custom.security.user.fullname')" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_jobtitle" class="col-sm-2 control-label">@lang('custom.security.user.jobtitle')</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="in_jobtitle" name="in_jobtitle" placeholder="@lang('custom.security.user.jobtitle')">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_email" class="col-sm-2 control-label">@lang('custom.security.user.email')</label>

                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="in_email" name="in_email" placeholder="@lang('custom.security.user.email')" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_phone" class="col-sm-2 control-label">@lang('custom.security.user.phone')</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="in_phone" name="in_phone" placeholder="@lang('custom.security.user.phone')" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_roleid" class="col-sm-2 control-label">@lang('custom.security.user.role')</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="in_roleid" id="in_roleid_add">
                                        <option value="">Choose the Option</option>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}">{{$role->role_desc}}</option>
                                        @endforeach
                                    </select>
                                    <div class="alert alert-danger" id="alert-danger-add" style="display: none">
                                        <ul>
                                            <li>
                                                <span id="select-error-add"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button id="resetAdd" type="button" class="btn btn-default">@lang('custom.reset')</button>
                            <button id="addButton" type="submit" class="btn btn-info pull-right">@lang('custom.save')</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>

        </div>
    </div>

    <!--Edit Modal -->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('custom.security.user.edit')</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{route('editSaveUser')}}" method="POST" id="editUserForm">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        @if(!empty(session('users')))
                            <input type="hidden" name="in_userid" value="{{session('users')->id}}" />
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="in_username" class="col-sm-2 control-label">@lang('custom.security.user.username')</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="in_username" name="in_username" placeholder="@lang('custom.security.user.username')"
                                              value="{{session('users')->username}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="in_fullname" class="col-sm-2 control-label">@lang('custom.security.user.fullname')</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="in_fullname" name="in_fullname" placeholder="@lang('custom.security.user.fullname')"
                                               value="{{session('users')->fullname}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="in_jobtitle" class="col-sm-2 control-label">@lang('custom.security.user.jobtitle')</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="in_jobtitle" name="in_jobtitle" placeholder="@lang('custom.security.user.jobtitle')"
                                               value="{{session('users')->job_title}}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="in_email" class="col-sm-2 control-label">@lang('custom.security.user.email')</label>

                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="in_email" name="in_email" placeholder="@lang('custom.security.user.email')"
                                               value="{{session('users')->email}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="in_phone" class="col-sm-2 control-label">@lang('custom.security.user.phone')</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="in_phone" name="in_phone" placeholder="@lang('custom.security.user.phone')"
                                               value="{{session('users')->phone}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="in_roleid" class="col-sm-2 control-label">@lang('custom.security.user.role')</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="in_roleid" id="in_roleid_edit">
                                            <option value="">Choose the Option</option>
                                            @foreach(session('roles') as $role)
                                                <option
                                                        @if($role->id == session('users')->role->id )
                                                            selected
                                                        @endif
                                                        value="{{$role->id}}">{{$role->role_desc}}</option>
                                            @endforeach
                                        </select>
                                        <div class="alert alert-danger" id="alert-danger-edit" style="display: none">
                                            <ul>
                                                <li>
                                                    <span id="select-error-edit"></span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        @endif
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button id="resetEdit" type="button" class="btn btn-default">@lang('custom.reset')</button>
                            <button id="editButton" type="submit" class="btn btn-info pull-right">@lang('custom.save')</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>

        </div>
    </div>

    <form name="editForm" id="editForm" action="{{route('editUser')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="hdn_userId" id="hdn_userId" value="0"/>
        <input type="hidden" name="isEdit" id="isEdit"
        @if(!empty(session('isEdit')))
               value="{{session('isEdit')}}"
         @endif
        />
    </form>
@stop
@push('scripts')
<script>
    $(function() {

        $(window).on('load',function(){
            var isEdit = document.getElementById("isEdit").value;
            if(isEdit==1){
                $('#editModal').modal('show');
            }
        });

        var dataTable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('userdata') !!}',
            aoColumnDefs:[
                {
                    "orderable": false,
                    "searchable": false,
                    "mDataProp" : "id",
                    "className" : "dt-center",
                    "mRender" : function(oObj) {
                        return '<input class=\"deleteRow\" type=\"checkbox\" name="chk_del" value=\"'+oObj+'\" >';
                    },
                    "aTargets" : [ 0 ]
                },
                {
                    "mDataProp" : "username",
                    "aTargets" : [ 1 ]
                },
                {
                    "mDataProp" : "fullname",
                    "aTargets" : [ 2 ]
                },
                {
                    "mDataProp" : "job_title",
                    "aTargets" : [ 3 ]
                },
                {
                    "mDataProp" : "email",
                    "aTargets" : [ 4 ]
                },
                {
                    "mDataProp" : "phone",
                    "aTargets" : [ 5 ]
                },
                {
                    "mDataProp" : "role_name",
                    "aTargets" : [ 6 ]
                },
                {
                    "mDataProp" : "created_at",
                    "aTargets" : [ 7 ]
                },
                {
                    "mDataProp" : "id",
                    "sDefaultContent" : "",
                    "className" : "dt-center",
                    "mRender" : function(oObj) {
                        return '<a class=\"btn btn-info btn-xs\" ' +
                            'href="#" onClick="javascript:edit('+oObj+')" title=\"Edit\" >' +
                            '<i class=\"fa fa-pencil-square-o\"><' +
                            '/i>' +
                            '</a>';
                    },
                    "aTargets" : [ 8 ]
                }
            ]
            });
        $("#bulkDelete").on('click',function() { // bulk checked
            var status = this.checked;
            $(".deleteRow").each( function() {
                $(this).prop("checked",status);
            });
        });
        $('#deleteTriger').on("click", function(event){ // triggering delete one by one
            if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
                if(confirm("Are you sure to delete this data?")==false){
                    return false;
                }
                var ids = [];
                $('.deleteRow').each(function(){
                    if($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });

              $.ajax({
                  type: "POST",
                  method: "POST",
                  headers: {
                      'X-CSRF-TOKEN': "{{csrf_token()}}"
                  },
                  url: "{{route('deleteuser')}}",
                  data: {data_ids:ids},
                  success: function(result) {
                      dataTable.draw(); // redrawing datatable
                  },
                    async:false
                });
            }else{
                alert('No Data Selected');
                return false;
            }
        });
        $("#addButton").on('click', function () {
            if ($('#in_roleid_add').val() == "") {
                $('#alert-danger-add').css("display", "block")
                document.getElementById("select-error-add").innerHTML = "Please Choose the Option";
                return false;
            }
        });
        $("#editButton").on('click', function () {
            if ($('#in_roleid_edit').val() == "") {
                $('#alert-danger-edit').css("display", "block")
                document.getElementById("select-error-edit").innerHTML = "Please Choose the Option";
                return false;
            }
        });

        $('#addUserForm').submit(function() {
            var c = confirm("Are you sure to save this data?");
            return c; //you can just return c because it will be true or false
        });

        $('#editUserForm').submit(function() {
            var c = confirm("Are you sure to save this data?");
            return c; //you can just return c because it will be true or false
        });
        
        $('#in_roleid_add').on("change", function(){
            if($('#in_roleid_add').val() == ""){
                $('#alert-danger-add').css("display", "block")
                document.getElementById("select-error-add").innerHTML = "Please Choose the Option";
            }else{
                $('#alert-danger-add').css("display", "none")
                document.getElementById("select-error-add").innerHTML = "";
            }
        });
        $('#in_roleid_edit').on("change", function(){
            if($('#in_roleid_edit').val() == ""){
                $('#alert-danger-edit').css("display", "block")
                document.getElementById("select-error-edit").innerHTML = "Please Choose the Option";
            }else{
                $('#alert-danger-edit').css("display", "none")
                document.getElementById("select-error-edit").innerHTML = "";
            }
        });

        $('#resetAdd').on('click',function() {
            document.getElementById("addUserForm").reset();
        });

        $('#resetEdit').on('click',function() {
            document.getElementById("editUserForm").reset();
        });

        var password = document.getElementById("in_pass")
            , confirm_password = document.getElementById("in_confpass");

        function validatePassword(){
            if(password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Passwords Don't Match");
            } else {
                confirm_password.setCustomValidity('');
            }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    });

    function edit(userid){
        document.getElementById("hdn_userId").value = userid;
        document.getElementById("editForm").submit()
    }
</script>
@endpush