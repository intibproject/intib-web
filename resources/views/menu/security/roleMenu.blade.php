@extends('layouts.layout')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if($menuAccess->isWrite=='Yes')
        <a class="btn bg-green btn-app" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-plus"></i> @lang('custom.security.rolemenu.add')
        </a>
    @endif
    @if($menuAccess->isDelete=='Yes')
        <a class="btn bg-red btn-app" id="deleteTriger">
            <i class="fa fa-times"></i> @lang('custom.security.rolemenu.delete')
        </a>
    @endif
    <div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-hover" id="rolemenus-table">
                <thead>
                <tr>
                    <th><input type="checkbox" name="bulkDelete" id="bulkDelete"/></th>
                    <th>@lang('custom.security.role.name')</th>
                    <th>@lang('custom.security.menu.name')</th>
                    <th>@lang('custom.security.rolemenu.isread')</th>
                    <th>@lang('custom.security.rolemenu.iswrite')</th>
                    <th>@lang('custom.security.rolemenu.isdelete')</th>
                    <th>@lang('custom.action')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Add Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('custom.security.rolemenu.add')</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{route('addSaveRoleMenu')}}" method="POST" id="addRoleMenuForm">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="in_rolename"
                                       class="col-sm-2 control-label">@lang('custom.security.role.name')</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="in_rolename" id="in_rolename" required>
                                        <option value="">Choose the Option</option>
                                        @foreach($roles as $role)
                                            <option
                                                    value="{{$role->id}}">{{$role->role_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_menuname"
                                       class="col-sm-2 control-label">@lang('custom.security.menu.name')</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="in_menuname" id="in_menuname" required>
                                        <option value="">Choose the Option</option>
                                        @foreach($menus as $menu)
                                            <option
                                                    value="{{$menu->id}}">{{$menu->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_isread"
                                       class="col-sm-2 control-label">@lang('custom.security.rolemenu.isread')</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="in_isread" id="in_isread" required>
                                        <option value="">Choose the Option</option>
                                        <option value="@lang('custom.yes')">@lang('custom.yes')</option>
                                        <option value="@lang('custom.no')">@lang('custom.no')</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_iswrite"
                                       class="col-sm-2 control-label">@lang('custom.security.rolemenu.iswrite')</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="in_iswrite" id="in_iswrite" required>
                                        <option value="">Choose the Option</option>
                                        <option value="@lang('custom.yes')">@lang('custom.yes')</option>
                                        <option value="@lang('custom.no')">@lang('custom.no')</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_isdelete"
                                       class="col-sm-2 control-label">@lang('custom.security.rolemenu.isdelete')</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="in_isdelete" id="in_isdelete" required>
                                        <option value="">Choose the Option</option>
                                        <option value="@lang('custom.yes')">@lang('custom.yes')</option>
                                        <option value="@lang('custom.no')">@lang('custom.no')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button id="resetAdd" type="button" class="btn btn-default">@lang('custom.reset')</button>
                            <button id="addbutton" type="submit"
                                    class="btn btn-info pull-right">@lang('custom.save')</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>

        </div>
    </div>

    <!--Edit Modal -->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('custom.security.role.edit')</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{route('editSaveRoleMenu')}}" method="POST" id="editRoleMenuForm">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        @if(!empty(session('rolemenus')))
                            <input type="hidden" name="in_id" value="{{session('rolemenus')->id}}"/>
                            <div class="modal-body">
                                <form class="form-horizontal" action="{{route('addSaveRoleMenu')}}" method="POST">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="in_rolename"
                                                   class="col-sm-2 control-label">@lang('custom.security.role.name')</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="in_rolename" id="in_rolename" required>
                                                    <option value="">Choose the Option</option>
                                                    @foreach(session('roles') as $role)
                                                        <option
                                                                @if($role->id == session('rolemenus')->role->id )
                                                                selected
                                                                @endif
                                                                value="{{$role->id}}">{{$role->role_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="in_menuname"
                                                   class="col-sm-2 control-label">@lang('custom.security.menu.name')</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="in_menuname" id="in_menuname" required>
                                                    <option value="">Choose the Option</option>
                                                    @foreach(session('menus') as $menu)
                                                        <option
                                                                @if($menu->id == session('rolemenus')->menu->id )
                                                                selected
                                                                @endif
                                                                value="{{$menu->id}}">{{$menu->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="in_isread"
                                                   class="col-sm-2 control-label">@lang('custom.security.rolemenu.isread')</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="in_isread" id="in_isread" required>
                                                    <option value="">Choose the Option</option>
                                                    <option
                                                            @if(session('rolemenus')->isRead == 'Yes' )
                                                                    selected
                                                            @endif
                                                            value="@lang('custom.yes')">@lang('custom.yes')</option>
                                                    <option @if(session('rolemenus')->isRead == 'No' )
                                                            selected
                                                            @endif
                                                            value="@lang('custom.no')">@lang('custom.no')</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="in_iswrite"
                                                   class="col-sm-2 control-label">@lang('custom.security.rolemenu.iswrite')</label>

                                            <div class="col-sm-10">
                                                <select class="form-control" name="in_iswrite" id="in_iswrite" required>
                                                    <option value="">Choose the Option</option>
                                                    <option
                                                            @if(session('rolemenus')->isRead == 'Yes' )
                                                            selected
                                                            @endif
                                                            value="@lang('custom.yes')">@lang('custom.yes')</option>
                                                    <option @if(session('rolemenus')->isRead == 'No' )
                                                            selected
                                                            @endif
                                                            value="@lang('custom.no')">@lang('custom.no')</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="in_isdelete"
                                                   class="col-sm-2 control-label">@lang('custom.security.rolemenu.isdelete')</label>

                                            <div class="col-sm-10">
                                                <select class="form-control" name="in_isdelete" id="in_isdelete" required>
                                                    <option value="">Choose the Option</option>
                                                    <option
                                                            @if(session('rolemenus')->isRead == 'Yes' )
                                                            selected
                                                            @endif
                                                            value="@lang('custom.yes')">@lang('custom.yes')</option>
                                                    <option @if(session('rolemenus')->isRead == 'No' )
                                                            selected
                                                            @endif
                                                            value="@lang('custom.no')">@lang('custom.no')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button id="resetEdit" type="button" class="btn btn-default">@lang('custom.reset')</button>
                                        <button id="editButton" type="submit"
                                                class="btn btn-info pull-right">@lang('custom.save')</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        @endif
                    </form>
                </div>
            </div>

        </div>
    </div>

    <form name="editForm" id="editForm" action="{{route('editRoleMenu')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="hdn_id" id="hdn_id" value="0"/>
        <input type="hidden" name="isEdit" id="isEdit"
               @if(!empty(session('isEdit')))
               value="{{session('isEdit')}}"
                @endif
        />
    </form>
@stop

@push('scripts')
<script>
    $(function () {

        $(window).on('load', function () {
            var isEdit = document.getElementById("isEdit").value;
            if (isEdit == 1) {
                $('#editModal').modal('show');
            }
        });

        var dataTable = $('#rolemenus-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('rolemenudata') !!}',
            aoColumnDefs: [
                {
                    "orderable": false,
                    "searchable": false,
                    "mDataProp": "id",
                    "sName": "role_menu.id",
                    "className": "dt-center",
                    "mRender": function (oObj) {
                        return '<input class=\"deleteRow\" type=\"checkbox\" name="chk_del" value=\"' + oObj + '\" >';
                    },
                    "aTargets": [0]
                },
                {
                    "sName": "role.role_name",
                    "mDataProp": "role_name",
                    "aTargets": [1]
                },
                {
                    "sName": "sys_menu.title",
                    "mDataProp": "title",
                    "aTargets": [2]
                },
                {
                    "sName": "role_menu.isRead",
                    "mDataProp": "isRead",
                    "aTargets": [3]
                },
                {
                    "sName": "role_menu.isWrite",
                    "mDataProp": "isWrite",
                    "aTargets": [4]
                },
                {
                    "sName": "role_menu.isDelete",
                    "mDataProp": "isDelete",
                    "aTargets": [5]
                },
                {
                    "sName": "role_menu.id",
                    "mDataProp": "id",
                    "sDefaultContent": "",
                    "className": "dt-center",
                    "mRender": function (oObj) {
                        return '<a class=\"btn btn-info btn-xs\" ' +
                            'href="#" onClick="javascript:edit(' + oObj + ')" title=\"Edit\" >' +
                            '<i class=\"fa fa-pencil-square-o\"><' +
                            '/i>' +
                            '</a>';
                    },
                    "aTargets": [6]
                }
            ]
        });
        $("#bulkDelete").on('click', function () { // bulk checked
            var status = this.checked;
            $(".deleteRow").each(function () {
                $(this).prop("checked", status);
            });
        });
        $('#deleteTriger').on("click", function (event) { // triggering delete one by one
            if ($('.deleteRow:checked').length > 0) {  // at-least one checkbox checked
                if(confirm("Are you sure to delete this data?")==false){
                    return false;
                }
                var ids = [];
                $('.deleteRow').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });

                $.ajax({
                    type: "POST",
                    method: "POST",
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    url: "{{route('deleteRoleMenu')}}",
                    data: {data_ids: ids},
                    success: function (result) {
                        dataTable.draw(); // redrawing datatable
                    },
                    async: false
                });
            }else{
                alert('No Data Selected');
                return false;
            }
        });
        $('#addRoleMenuForm').submit(function() {
            var c = confirm("Are you sure to save this data?");
            return c; //you can just return c because it will be true or false
        });

        $('#editRoleMenuForm').submit(function() {
            var c = confirm("Are you sure to save this data?");
            return c; //you can just return c because it will be true or false
        });

        $('#resetAdd').on('click',function() {
            document.getElementById("addRoleMenuForm").reset();
        });

        $('#resetEdit').on('click',function() {
            document.getElementById("editRoleMenuForm").reset();
        });
    });

    function edit(id) {
        document.getElementById("hdn_id").value = id;
        document.getElementById("editForm").submit()
    }
</script>
@endpush
