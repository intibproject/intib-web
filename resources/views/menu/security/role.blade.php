@extends('layouts.layout')


@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($menuAccess->isWrite=='Yes')
        <a class="btn bg-green btn-app" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-plus"></i> @lang('custom.security.role.add')
        </a>
    @endif
    @if($menuAccess->isDelete=='Yes')
        <a class="btn bg-red btn-app" id="deleteTriger">
            <i class="fa fa-times"></i> @lang('custom.security.role.delete')
        </a>
    @endif
    <div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-hover" id="roles-table">
                <thead>
                <tr>
                    <th><input type="checkbox" name="bulkDelete" id="bulkDelete"/></th>
                    <th>@lang('custom.security.role.name')</th>
                    <th>@lang('custom.security.role.desc')</th>
                    <th>@lang('custom.registereddate')</th>
                    <th>@lang('custom.action')</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Add Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('custom.security.role.add')</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{route('addSaveRole')}}" method="POST" id="addRoleForm">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="in_rolename"
                                       class="col-sm-2 control-label">@lang('custom.security.role.name')</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="in_rolename" name="in_rolename"
                                           placeholder="@lang('custom.security.role.name')" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="in_roledesc"
                                       class="col-sm-2 control-label">@lang('custom.security.role.desc')</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="in_roledesc" name="in_roledesc"
                                           placeholder="@lang('custom.security.role.desc')" required>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button id="resetAdd" type="button" class="btn btn-default">@lang('custom.reset')</button>
                            <button id="buttonAdd" type="submit"
                                    class="btn btn-info pull-right">@lang('custom.save')</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>

        </div>
    </div>

    <!--Edit Modal -->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('custom.security.role.edit')</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{route('editSaveRole')}}" method="POST" id="editRoleForm">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        @if(!empty(session('roles')))
                            <input type="hidden" name="in_id" value="{{session('roles')->id}}"/>
                            <div class="modal-body">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="in_rolename"
                                                   class="col-sm-2 control-label">@lang('custom.security.role.name')</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="in_rolename"
                                                       name="in_rolename" value="{{session('roles')->role_name}}"
                                                       placeholder="@lang('custom.security.role.name')" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="in_roledesc"
                                                   class="col-sm-2 control-label">@lang('custom.security.role.desc')</label>

                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="in_roledesc"
                                                       name="in_roledesc" value="{{session('roles')->role_desc}}"
                                                       placeholder="@lang('custom.security.role.desc')" required>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button id="resetEdit" type="button" class="btn btn-default">@lang('custom.reset')</button>
                                        <button id="buttonEdit" type="submit"
                                                class="btn btn-info pull-right">@lang('custom.save')</button>
                                    </div>
                                    <!-- /.box-footer -->
                            </div>
                        @endif
                    </form>
                </div>
            </div>

        </div>
    </div>

    <form name="editForm" id="editForm" action="{{route('editRole')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="hdn_id" id="hdn_id" value="0"/>
        <input type="hidden" name="isEdit" id="isEdit"
               @if(!empty(session('isEdit')))
               value="{{session('isEdit')}}"
                @endif
        />
    </form>
@stop

@push('scripts')
<script>
    $(function () {

        $(window).on('load', function () {
            var isEdit = document.getElementById("isEdit").value;
            if (isEdit == 1) {
                $('#editModal').modal('show');
            }
        });

        var dataTable = $('#roles-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('roledata') !!}',
            aoColumnDefs: [
                {
                    "orderable": false,
                    "searchable": false,
                    "mDataProp": "id",
                    "className": "dt-center",
                    "mRender": function (oObj) {
                        return '<input class=\"deleteRow\" type=\"checkbox\" name="chk_del" value=\"' + oObj + '\" >';
                    },
                    "aTargets": [0]
                },
                {
                    "mDataProp": "role_name",
                    "aTargets": [1]
                },
                {
                    "mDataProp": "role_desc",
                    "aTargets": [2]
                },
                {
                    "mDataProp": "created_at",
                    "aTargets": [3]
                },
                {
                    "mDataProp": "id",
                    "sDefaultContent": "",
                    "className": "dt-center",
                    "mRender": function (oObj) {
                        return '<a class=\"btn btn-info btn-xs\" ' +
                            'href="#" onClick="javascript:edit(' + oObj + ')" title=\"Edit\" >' +
                            '<i class=\"fa fa-pencil-square-o\"><' +
                            '/i>' +
                            '</a>';
                    },
                    "aTargets": [4]
                }
            ]
        });
        $("#bulkDelete").on('click', function () { // bulk checked
            var status = this.checked;
            $(".deleteRow").each(function () {
                $(this).prop("checked", status);
            });
        });
        $('#deleteTriger').on("click", function (event) { // triggering delete one by one
            if ($('.deleteRow:checked').length > 0) {  // at-least one checkbox checked
                if(confirm("Are you sure to delete this data?")==false){
                    return false;
                }
                var ids = [];
                $('.deleteRow').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });

                $.ajax({
                    type: "POST",
                    method: "POST",
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    url: "{{route('deleteRole')}}",
                    data: {data_ids: ids},
                    success: function (result) {
                        dataTable.draw(); // redrawing datatable
                    },
                    async: false
                });
            }else{
                alert('No Data Selected');
                return false;
            }
        });

        $('#addRoleForm').submit(function() {
            var c = confirm("Are you sure to save this data?");
            return c; //you can just return c because it will be true or false
        });

        $('#editRoleForm').submit(function() {
            var c = confirm("Are you sure to save this data?");
            return c; //you can just return c because it will be true or false
        });

        $('#resetAdd').on('click',function() {
            document.getElementById("addRoleForm").reset();
        });

        $('#resetEdit').on('click',function() {
            document.getElementById("editRoleForm").reset();
        });
    });

    function edit(id) {
        document.getElementById("hdn_id").value = id;
        document.getElementById("editForm").submit()
    }
</script>
@endpush