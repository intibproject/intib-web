@extends('layouts.layout')

@push('styles')
<style>
    #row_img{
        background-color: #222D32;
    }
    .logo-lg{
        font-size: 15px;
    }


</style>
@endpush

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="box box-default box-solid">
                <div class="box-header" style="background-color: #222D32;color:white">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title">
                        Welcome, {{Auth::user()->username}}
                    </h3>
                </div>
                <div class="box-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th >Last Login </th>
                            <th > : </th>
                            <td>{{$last_login}}</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th >Time </th>
                            <th > : </th>
                            <td>{{$today}}</td>
                        </tr>
                        <tr>
                            <th >Role </th>
                            <th > : </th>
                            <td>{{$rolename->role_name}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3" id="">
            <center><img src="" width="40%" height="40%"  style=""></center>
        </div>
    </div>
</div>
@endsection