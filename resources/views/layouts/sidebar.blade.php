
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
        <li class="header">Main Menu</li>
        <!-- Optionally, you can add icons to the links -->
        @foreach($menus as $parent_item)
            @if($parent_item->level == 1 and $parent_item->parent_id == 0)
                @if( $parent_item->id == $menuName->id)
                    <li class="active">
                @else
                    <li>
                        @endif
                        <a href="{{ route($parent_item->link) }}"><i class="{{$parent_item->icon}}"></i> <span>{{$parent_item->name}}</span></a></li>
                @endif
                @if(!($parent_item->level == 1))
                    @if($parent_item->id == $menuName->parent_id)
                        <li class="treeview active">
                    @else
                        <li class="treeview">
                            @endif
                            <a href="#"><i class="{{$parent_item->icon}}"></i> <span>{{$parent_item->name}}</span>
                                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                            </a>
                            <ul class="treeview-menu">
                                @foreach($menus->where('parent_id', $parent_item->id) as $child_item)
                                    @if($child_item->id == $menuName->id)
                                        <li class="active">
                                    @else
                                        <li>
                                            @endif
                                            <a href="{{ route($child_item->link) }}"><i class="{{$child_item->icon}}"></i>{{$child_item->name}}</a></li>
                                        @endforeach
                            </ul>
                        </li>
                    @endif
        @endforeach
    </ul>
    <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->