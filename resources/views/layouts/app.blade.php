<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .navbar-brand{
            color: yellow;
            font-size: 1.5em;
        }
        .navbar{
            background-color:black;
            border: none;
            margin-bottom: 50px
        }
        img{
            animation: fadein 3s;
            -moz-animation: fadein 3s;
            -webkit-animation: fadein 3s;
            -o-animation: fadein 3s;
        }

        @keyframes fadein {
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-moz-keyframes fadein {
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-webkit-keyframes fadein {
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-o-keyframes fadein {
            from {
                opacity:0;
            }
            to {
                opacity: 1;
            }
        }
    </style>
</head>
<body background="{{asset('img/background.jpg')}}">
    <div id="app">
        <center><img src="" width="20%" height="20%"  style="margin-bottom: 10px"></center>
        @yield('content')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
