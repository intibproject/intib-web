<?php
/**
 * Created by PhpStorm.
 * User: fadhilla.hentino
 * Date: 27/04/2017
 * Time: 21:42
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Reset',
    'save' => 'Save',
    'registereddate' => 'Registered Date',
    'action' => 'Action',
    'yes' => 'Yes',
    'no' => 'No',
    'nonactive' => 'NON ACTIVE',
    'active' => 'ACTIVE',
    'scehduleNotFound' => 'Schedule Not Found ' ,
    'download.report' => ' Report',
    'status.extend' => 'Extend',
    'status.registration' => 'Registration',
    'print' => 'Print',
    'delete'=> 'Delete Data',
    'add'=> 'Add New Data',
    'confirm.delete' => 'Are you sure to delete this data?',
    'descr' => 'Description',

    'studentName' => 'Student Name',
    'parentsName' => 'Parents Name',
    'phone' => 'Phone',
    'email' => 'Email',
    'className' => 'Classes Name',
    'reminderDate' => 'Reminder Date',
    'notes' => 'Notes',
    'contact' => 'Contact',
    'repeat' => 'Repeat',
    'item' => 'Item',
    'itemName' => 'Item Name',
    'itemDesc' => 'Item Description',
    'itemPrice' => 'Price Per Item',
    'date' => 'Date',
    'name' => 'Name',
    'status' => 'Status',
    'qty' => 'Qty',
    'stock' => 'Stock',
    'startDate' => 'Start Date',
    'endDate' => 'End Date',
    'reason' => 'Reason',
    'totalMonth' => 'Total Month',
    'leaveStartDate' => 'Leave Start Date',
    'leaveEndDate' => 'Leave End Date',
    'leaveFeeMonth' => 'Leave Fee Per Month',
    'totalPay' => 'Total Payment',
    'address' => 'Address',
    'term' => 'Term',
    'photo' => 'Photo',
    'price' => 'Price',
    'registrationFee' => 'Registration Fee',
    'disc' => 'Discount',
    'paid' => 'Paid',
    'payDate' => 'Payment Date',
    'schedule' => 'Schedule',
    'othPay' => 'Other Payment',
    'cancel' => 'Cancel',
    'payStatus' => 'Payment Status',
    'termPeriod' => 'Term Period',
    'totalTerm' => 'Total Term',
    'termPrice' => 'Price Per Term',
    'totalPrice' => 'Total Price',
    'study' => 'Study',
    'schDay' => 'Schedule Day',
    'schTime' => 'Schedule Time',
    'teacher' => 'Teacher',
    'numberStudent' => 'Number Of Student',

    /*hardcode*/
    'repeat.once'  => 'once',
    'repeat.monthly'  => 'monthly',
    'status.IN'=> 'IN',
    'status.OUT'=> 'OUT',
    'status.NONACTIVE'=> 'NON ACTIVE',
    'status.ACTIVE'=> 'ACTIVE',
    'status.QUARTERLY'=> 'Quartely',
    'status.MONTHLY'=> 'Monthly',
    'status.DAILY'=> 'Daily',


    /*Security*/
    'security.user.add' => 'Add User',
    'security.user.edit' => 'Edit User',
    'security.user.delete' => 'Delete User',
    'security.user.username' => 'UserName',
    'security.user.password' => 'Password',
    'security.user.confirmpassword' => 'Confirm Password',
    'security.user.fullname' => 'Full Name',
    'security.user.jobtitle' => 'Job Title',
    'security.user.email' => 'E-mail',
    'security.user.phone' => 'Phone',
    'security.user.role' => 'Role',


    'security.role.add' => 'Add Role',
    'security.role.edit' => 'Edit Role',
    'security.role.delete' => 'Delete Role',
    'security.role.name' => 'Role Name',
    'security.role.desc' => 'Description',


    'security.rolemenu.add' => 'Add Role Menu',
    'security.rolemenu.edit' => 'Edit Role Menu',
    'security.rolemenu.delete' => 'Delete Role Menu',
    'security.rolemenu.isread' => 'IsRead',
    'security.rolemenu.iswrite' => 'IsWrite',
    'security.rolemenu.isdelete' => 'IsDelete',

    'security.menu.name' => 'Menu Name',

];