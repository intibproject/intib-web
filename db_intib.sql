-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 22, 2017 at 06:44 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_intib`
--

-- --------------------------------------------------------

--
-- Table structure for table `param_setting`
--

CREATE TABLE `param_setting` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_setting`
--

INSERT INTO `param_setting` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'Extend Date', '7', '2017-05-15 00:00:00', '2017-05-15 00:00:00'),
(2, 'Registration Fee', '10000', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `role_desc` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role_name`, `role_desc`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'Admin', '2017-04-23 00:00:00', '0000-00-00 00:00:00'),
(2, 'Operator', 'Operators', '2017-04-23 00:00:00', '2017-05-25 15:16:40');

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `isRead` varchar(3) NOT NULL,
  `isWrite` varchar(3) NOT NULL,
  `isDelete` varchar(3) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id`, `role_id`, `menu_id`, `isRead`, `isWrite`, `isDelete`, `updated_at`, `created_at`) VALUES
(4, 1, 2, 'No', 'No', 'No', '2017-05-29 00:35:20', '0000-00-00 00:00:00'),
(5, 1, 4, 'Yes', 'Yes', 'Yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 1, 3, 'Yes', 'Yes', 'Yes', '2017-05-29 00:31:59', '2017-05-29 00:31:59'),
(21, 1, 18, 'Yes', 'Yes', 'Yes', '2017-09-10 12:11:12', '2017-09-10 12:11:12'),
(22, 1, 19, 'Yes', 'No', 'Yes', '2017-09-18 13:03:31', '2017-09-18 01:13:36'),
(23, 1, 21, 'Yes', 'Yes', 'Yes', '2017-09-18 01:49:23', '2017-09-18 01:49:23'),
(24, 1, 22, 'Yes', 'Yes', 'Yes', '2017-09-18 01:50:01', '2017-09-18 01:50:01'),
(25, 1, 6, 'Yes', 'Yes', 'Yes', '2017-09-26 00:26:28', '2017-09-26 00:26:28'),
(26, 1, 14, 'Yes', 'Yes', 'Yes', '2017-09-26 01:16:11', '2017-09-26 01:16:11');

-- --------------------------------------------------------

--
-- Table structure for table `sys_menu`
--

CREATE TABLE `sys_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `display_order` int(11) NOT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sys_menu`
--

INSERT INTO `sys_menu` (`id`, `parent_id`, `name`, `title`, `description`, `link`, `level`, `display_order`, `icon`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'Security', 'Security', '', 'home', 0, 25, 'fa fa-lock', NULL, NULL, NULL),
(2, 1, 'Users', 'Users', '', 'security/users', 1, 26, 'fa fa-circle-o text-aqua', NULL, NULL, NULL),
(3, 1, 'Role', 'Role', '', 'security/roles', 1, 27, 'fa fa-circle-o text-aqua', NULL, NULL, NULL),
(4, 1, 'Role Menu', 'Role Menu', '', 'security/rolemenu', 1, 28, 'fa fa-circle-o text-aqua', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `job_title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `fullname`, `job_title`, `email`, `password`, `phone`, `remember_token`, `created_at`, `updated_at`, `role_id`, `last_login`) VALUES
(5, 'fadhilfcr7', 'fadhilla eka', 'Administrator', 'fadhilfcr@gmail.com', '$2y$10$f6qUxdNiv5dfV8AmII0nUuG1aXQ//UKjG54/WUoh.JHdUo6y7Y4r6', '021-20020051', 'PBQwcKYx4mOGKvp80CiaNHJKPUs35BDwUO7dDwZUqEr9FypNCSlB8Pc0G1KU', '2017-04-22 17:00:00', '2017-09-16 04:15:12', 1, '2017-09-16 11:15:12'),
(6, 'admin', 'admin', 'Administrator', 'admin@gmail.com', '$2y$10$f6qUxdNiv5dfV8AmII0nUuG1aXQ//UKjG54/WUoh.JHdUo6y7Y4r6', '021-20020051', 'aDs3jiJ8fbMYN0P0xrnMQFIwbiYHqDpWmQ246VTKEihDvnTZ1Ngjw4d7BMvY', '2017-04-22 17:00:00', '2017-11-22 17:44:25', 1, '2017-11-23 00:44:25'),
(8, 'operators', 'Operator 1', 'Operator', 'operator@gmail.com', '$2y$10$aP.jP5AiQIyxOhfBtGqJRuZS/WFMdlh1lsvcYWqjZ04vPQEYfAd2a', '021-123123123', 'ecDTDH1IjEFkNlq6cuVzKKQGlnb4IJzAb7tSsUOM', '2017-04-26 17:00:00', '2017-04-27 15:50:03', 2, '0000-00-00 00:00:00'),
(9, 'ninja', 'ninja', 'admin', 'a@aa', '$2y$10$EgtuXseMhXhKF.KajBKvDeMx3Yilt6TEW4zKUrOQwGNvxVBte/4Py', '123123', 'ecDTDH1IjEFkNlq6cuVzKKQGlnb4IJzAb7tSsUOM', '2017-04-27 15:14:08', '2017-04-27 15:14:08', 1, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `param_setting`
--
ALTER TABLE `param_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_menu`
--
ALTER TABLE `sys_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `param_setting`
--
ALTER TABLE `param_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role_menu`
--
ALTER TABLE `role_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `sys_menu`
--
ALTER TABLE `sys_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
