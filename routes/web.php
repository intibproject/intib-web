<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(empty(Auth::user()->id)){
        return view('auth/login');
    }else{
        return redirect('/home');
    }
});

Auth::routes();

    //Menu Security
    Route::get('/home', 'ChartController@index')->name('home');

    /*Data Menu User*/
    Route::get('/security/users', 'Menu\Security\UserController@index')->name('security/users');
    Route::get('/security/userdata', 'Menu\Security\UserController@anyData')->name('userdata');

    Route::post('/security/addSaveUser', 'Menu\Security\UserController@addSave')->name('addSaveUser');
    Route::post('/security/editSaveUser', 'Menu\Security\UserController@editSave')->name('editSaveUser');
    Route::post('/security/editUsers', 'Menu\Security\UserController@edit')->name('editUser');
    Route::post('/security/deleteUsers', 'Menu\Security\UserController@delete')->name('deleteuser');

    /*Data Menu Role*/
    Route::get('/security/roles', 'Menu\Security\RoleController@index')->name('security/roles');
    Route::get('/security/roledata', 'Menu\Security\RoleController@anyData')->name('roledata');

    Route::post('/security/addSave', 'Menu\Security\RoleController@addSave')->name('addSaveRole');
    Route::post('/security/editSave', 'Menu\Security\RoleController@editSave')->name('editSaveRole');
    Route::post('/security/editRoles', 'Menu\Security\RoleController@edit')->name('editRole');
    Route::post('/security/deleteRoles', 'Menu\Security\RoleController@delete')->name('deleteRole');

    /*Data Menu Role Menu*/
    Route::get('/security/rolemenu', 'Menu\Security\RoleMenuController@index')->name('security/rolemenu');
    Route::get('/security/rolemenudata', 'Menu\Security\RoleMenuController@anyData')->name('rolemenudata');

    Route::post('/security/addSaveRoleMenu', 'Menu\Security\RoleMenuController@addSave')->name('addSaveRoleMenu');
    Route::post('/security/editSaveRoleMenu', 'Menu\Security\RoleMenuController@editSave')->name('editSaveRoleMenu');
    Route::post('/security/editRoleMenus', 'Menu\Security\RoleMenuController@edit')->name('editRoleMenu');
    Route::post('/security/deleteRoleMenus', 'Menu\Security\RoleMenuController@delete')->name('deleteRoleMenu');