<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert(array(
            'username'     => 'fadhilfcr',
            'firstname'     => 'fadhilla eka',
            'lastname'     => 'hentino',
            'email'    => 'fadhilfcr@gmail.com',
            'password' => Hash::make('admin'),
            'roleid' => '1',
        ));
    }
}
