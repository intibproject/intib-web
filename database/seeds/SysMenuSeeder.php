<?php

use Illuminate\Database\Seeder;

class SysMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sys_menu')->delete();
        DB::table('sys_menu')->insert(array(
            'parent_id'     => '0',
            'name'     => 'Chart',
            'title'    => 'Chart',
            'link'    => '/home',
            'level'    => '0',
            'display_order'    => '0'
        ));
        DB::table('sys_menu')->insert(array(
            'parent_id'     => '0',
            'name'     => 'Data',
            'title'    => 'Data',
            'link'    => '',
            'level'    => '0',
            'display_order'    => '1'
        ));
        DB::table('sys_menu')->insert(array(
            'parent_id'     => '2',
            'name'     => 'List of Users',
            'title'    => 'List of Users',
            'link'    => '/users',
            'level'    => '1',
            'display_order'    => '2'
        ));
        DB::table('sys_menu')->insert(array(
            'parent_id'     => '2',
            'name'     => 'List of Destination',
            'title'    => 'List of Destination',
            'link'    => '/destinations',
            'level'    => '1',
            'display_order'    => '3'
        ));
    }
}
