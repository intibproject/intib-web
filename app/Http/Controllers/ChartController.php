<?php

namespace intib\Http\Controllers;

use Carbon\Carbon;
use intib\User;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::find(\Auth::user()->id);
        $last_login = Carbon::parse($users->last_login)->format('l jS \\of F Y h:i:s A');;
        $date = Carbon::now();
        $date = $date->format('l jS \\of F Y h:i:s A');
        $roleName = User::join('role','users.role_id','=','role.id')
            ->where('users.id','=',\Auth::user()->id)->first();
        return view('home')->with('last_login',$last_login)->with('today',$date)->with('rolename',$roleName);
    }
}
