<?php

namespace intib\Http\Controllers\Menu\Security;

use intib\Http\Controllers\Controller;
use intib\Model\security\Role;
use intib\Model\security\RoleMenu;
use intib\Model\SysMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

class RoleMenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $roles = Role::all();
        $menus = SysMenu::where('level','=',1)->get();
        return view('menu.security.roleMenu')->with('roles',$roles)->with('menus',$menus);;
    }

    public function anyData()
    {
        $rolemenus = RoleMenu::join('role','role_menu.role_id','=','role.id')->join('sys_menu','role_menu.menu_id','=','sys_menu.id')
            ->select(['role_menu.id', 'role_menu.isWrite', 'role_menu.isRead', 'role_menu.isDelete', 'role.role_name', 'sys_menu.title']);
        return Datatables::of($rolemenus)->make(true);
    }

    public function addSave(Request $request){
        $checkrolemenu = RoleMenu::where('role_id','=',Input::get("in_rolename"))->where('menu_id','=',Input::get("in_menuname"))->count();
        if($checkrolemenu>0){
            return redirect()->route('security/rolemenu')->withErrors(['error'=>'Role Menu Data already exists']);;
        }
        $rolemenu = new RoleMenu();
        $rolemenu->role_id = Input::get("in_rolename");
        $rolemenu->menu_id = Input::get("in_menuname");
        $rolemenu->isRead = Input::get("in_isread");
        $rolemenu->isWrite = Input::get("in_iswrite");
        $rolemenu->isDelete = Input::get("in_isdelete");
        $rolemenu->save();
        return redirect()->route('security/rolemenu');
    }

    public function edit(Request $request){
        $Id = Input::get("hdn_id");
        $rolemenus = RoleMenu::where('id','=',$Id)->first();
        $roles = Role::all();
        $menus = SysMenu::where('level','=',1)->get();
        return redirect()->route('security/rolemenu')->with(array('isEdit'=>1,'rolemenus'=>$rolemenus,'roles'=>$roles,'menus'=>$menus));
    }

    public function editSave(Request $request){
        $checkrolemenu = RoleMenu::where('role_id','=',Input::get("in_rolename"))->where('menu_id','=',Input::get("in_menuname"))
            ->where('id','!=',Input::get("in_id"))->count();
        if($checkrolemenu>0){
            return redirect()->route('security/rolemenu')->withErrors(['error'=>'Role Menu Data already exists']);;
        }
        $id = Input::get("in_id");
        $rolemenu = RoleMenu::find($id);
        $rolemenu->role_id = Input::get("in_rolename");
        $rolemenu->menu_id = Input::get("in_menuname");
        $rolemenu->isRead = Input::get("in_isread");
        $rolemenu->isWrite = Input::get("in_iswrite");
        $rolemenu->isDelete = Input::get("in_isdelete");
        $rolemenu->save();
        return redirect()->route('security/rolemenu');
    }


}
