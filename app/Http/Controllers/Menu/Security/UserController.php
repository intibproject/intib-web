<?php

namespace intib\Http\Controllers\Menu\Security;

use intib\Http\Controllers\Controller;
use intib\Model\security\Role;
use intib\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $roles = Role::all();
        return view('menu.security.user')->with('roles',$roles);
    }
    public function anyData()
    {
            return Datatables::of(User::where('id','!=',\Auth::user()->id) ->get())
                ->addColumn('role_name', function (User $user) {
                    return $user->role ? str_limit($user->role->role_name, 30, '...') : '';
                })
                ->make(true);
    }

    public function addSave(Request $request){
        $checkuser = User::where('username','=',Input::get("in_username"))->count();
        if($checkuser>0){
            return redirect()->route('security/users')->withErrors(['error'=>'Username already exists']);;
        }
        $checkmail = User::where('email','=',Input::get("in_email"))->count();
        if($checkmail>0){
            return redirect()->route('security/users')->withErrors(['error'=>'Email already exists']);;
        }
        $user = new User();
        $user->username = Input::get("in_username");
        $user->fullname = Input::get("in_fullname");
        $user->password = Hash::make(Input::get("in_pass"));
        $user->job_title = Input::get("in_jobtitle");
        $user->email = Input::get("in_email");
        $user->phone = Input::get("in_phone");
        $user->role_id = Input::get("in_roleid");
        $user->remember_token = Input::get("_token");
        $user->save();
        return redirect()->route('security/users');
    }

    public function edit(Request $request){
        $roles = Role::all();
        $userId = Input::get("hdn_userId");
        $users = User::where('id','=',$userId)->first();
        return redirect()->route('security/users')->with(array('users'=>$users,'isEdit'=>1,'roles'=>$roles));
    }

    public function editSave(Request $request){

        $checkuser = User::where('username','=',Input::get("in_username"))->where('id','!=',Input::get("in_userid"))->count();
        if($checkuser>0){
            return redirect()->route('security/users')->withErrors(['error'=>'Username already exists']);;
        }
        $checkmail = User::where('email','=',Input::get("in_email"))->where('id','!=',Input::get("in_userid"))->count();
        if($checkmail>0){
            return redirect()->route('security/users')->withErrors(['error'=>'Email already exists']);;
        }

        $userid = Input::get("in_userid");
        $user = User::find($userid);
        $user->username = Input::get("in_username");
        $user->fullname = Input::get("in_fullname");
        $user->password = Hash::make(Input::get("in_pass"));
        $user->job_title = Input::get("in_jobtitle");
        $user->email = Input::get("in_email");
        $user->phone = Input::get("in_phone");
        $user->role_id = Input::get("in_roleid");
        $user->remember_token = Input::get("_token");
        $user->save();
        return redirect()->route('security/users');
    }

    public function delete(Request $request){
        $ids = $request->input('data_ids');
        User::whereIn('id',$ids)->delete();
    }
}
