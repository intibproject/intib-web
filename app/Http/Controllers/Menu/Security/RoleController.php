<?php

namespace intib\Http\Controllers\Menu\Security;

use intib\Http\Controllers\Controller;
use intib\Model\security\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('menu.security.role');
    }
    public function anyData()
    {
        return Datatables::of(Role::where('id','!=',\Auth::user()->role_id) ->get())->make(true);
    }

    public function addSave(Request $request){

        $checkrole = Role::where('role_name','=',Input::get("in_rolename"))->count();
        if($checkrole>0){
            return redirect()->route('security/roles')->withErrors(['error'=>'Role Name already exists']);;
        }
        $role = new Role();
        $role->role_name = Input::get("in_rolename");
        $role->role_desc = Input::get("in_roledesc");
        $role->save();
        return redirect()->route('security/roles');
    }

    public function edit(Request $request){
        $roleId = Input::get("hdn_id");
        $roles = Role::where('id','=',$roleId)->first();
        return redirect()->route('security/roles')->with(array('isEdit'=>1,'roles'=>$roles));
    }

    public function editSave(Request $request){
        $checkrole = Role::where('role_name','=',Input::get("in_rolename"))->where('id','!=',Input::get("in_id"))->count();
        if($checkrole>0){
            return redirect()->route('security/roles')->withErrors(['error'=>'Role Name already exists']);;
        }
        $roleid = Input::get("in_id");
        $role = role::find($roleid);
        $role->role_name = Input::get("in_rolename");
        $role->role_desc = Input::get("in_roledesc");
        $role->save();
        return redirect()->route('security/roles');
    }

    public function delete(Request $request){
        $ids = $request->input('data_ids');
        Role::whereIn('id',$ids)->delete();
    }
}
