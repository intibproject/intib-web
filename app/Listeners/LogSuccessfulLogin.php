<?php

namespace intib\Listeners;

use intib\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $date = date("Y-m-d H:i:s");
        $users = User::find(\Auth::user()->id);
        $users->last_login = $date;
        $users->save();
    }
}
