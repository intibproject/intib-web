<?php

namespace intib\Providers;

use intib\Model\security\roleMenu;
use intib\Model\SysMenu;
use intib\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

            view()->composer('layouts.sidebar', function ($view) {
                $userid = Auth::user()->id;
                $roleid = User::find($userid)->role_id;
                $roleIds = roleMenu::where('role_id', '=', $roleid)->pluck('menu_id')->toArray();
                $parentIds = SysMenu::whereIn('id', $roleIds)->pluck('parent_id')->toArray();
                $menus = \intib\Model\SysMenu::whereIn('id', $roleIds)->orWhereIn('id', $parentIds)
                    ->orderBy('display_order', 'ASC')->get();
                $view->with('menus', $menus);
            });

            view()->composer('*', function ($view) {
                $current_route_name = \Request::route()->getName();
                $menuid = SysMenu::where('link', '=', $current_route_name)->pluck('id')->first();
                $menuName = SysMenu::where('link', '=', $current_route_name)->first();
                if(empty($menuName)){
                    $menuName = SysMenu::where('link', '=', 'home')->first();
                }

                $view->with('current_route_name', $current_route_name);
                $view->with('menuName', $menuName);
                if (!empty($menuid)) {
                    $userid = Auth::user()->id;
                    $roleid = User::find($userid)->role_id;
                    $roleMenu = roleMenu::where('role_id', '=', $roleid)->where('menu_id', '=', $menuid)->first();
                    if (!empty($roleMenu)) {
                        $view->with('menuAccess', $roleMenu);
                    } else {
                        $view->with('menuAccess', 'null');
                    }
                }else{
                    redirect('auth/login');
                }
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
