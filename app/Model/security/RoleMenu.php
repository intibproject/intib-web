<?php

namespace intib\Model\security;

use intib\Model\SysMenu;
use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    protected $table = 'role_menu';

    public function role(){
        return $this->belongsTo(Role::class, 'role_id','id');
    }
    public function menu(){
        return $this->belongsTo(SysMenu::class, 'menu_id','id');
    }
}
