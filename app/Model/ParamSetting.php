<?php

namespace intib\Model;

use Illuminate\Database\Eloquent\Model;

class ParamSetting extends Model
{
    protected $table = "param_setting";
}
